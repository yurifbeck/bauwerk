import initialState from './state'

export default {
  RESET_STORE: (state) => {
    Object.assign(state, initialState())
  },

  SET_AUTH_USER: (state, { authUser }) => {
    state.authUser = {
      uid: authUser.uid,
      email: authUser.email,
    }
  },

  SET_OBRA: (state, { doc }) => {
    state.obra = {
      id: doc.id,
      nome: doc.data().nome,
    }
  },
}
