/* eslint-disable no-throw-literal */
export default {
  async nuxtServerInit({ dispatch }, ctx) {
    if (this.$fireAuth === null) {
      throw 'nuxtServerInit Example not working - this.$fireAuth cannot be accessed.'
    }

    if (ctx.$fireAuth === null) {
      throw 'nuxtServerInit Example not working - ctx.$fireAuth cannot be accessed.'
    }

    if (ctx.app.$fireAuth === null) {
      throw 'nuxtServerInit Example not working - ctx.$fireAuth cannot be accessed.'
    }

    // INFO -> Nuxt-fire Objects can be accessed in nuxtServerInit action via this.$fire___, ctx.$fire___ and ctx.app.$fire___'

    /** Get the VERIFIED authUser from the server */
    if (ctx.res && ctx.res.locals && ctx.res.locals.user) {
      const { allClaims: claims, ...authUser } = ctx.res.locals.user

      console.info(
        'Auth User verified on server-side. User: ',
        authUser,
        'Claims:',
        claims
      )

      await dispatch('onAuthStateChanged', {
        authUser,
        claims,
      })
    }
  },

  async onAuthStateChanged({ dispatch, commit }, { authUser }) {
    if (!authUser) {
      commit('RESET_STORE')
      return
    }
    commit('SET_AUTH_USER', { authUser })
    const userRef = this.$fireStore.collection('users')
    try {
      await userRef
        .doc(authUser.uid)
        .get()
        .then((doc) => {
          if (doc.exists) {
            const obraId = doc.data().favorite
            dispatch('updateObra', { obraId })
          } else {
            alert('error, no such document')
          }
        })
    } catch (e) {
      alert(e)
    }
    this.$router.push('inicio')
  },

  async updateObra({ commit }, { obraId }) {
    if (this.$fireAuth === null) {
      throw 'Vuex Store example not working - this.$fireAuth cannot be accessed.'
    } else {
      try {
        const obrasRef = this.$fireStore.collection('obras')
        console.log('obra id', obraId)
        await obrasRef
          .doc(obraId)
          .get()
          .then((doc) => {
            if (doc.exists) {
              commit('SET_OBRA', { doc })
            } else {
              alert('error, no such document')
            }
          })
      } catch (e) {
        alert(e)
      }
    }
  },
}
